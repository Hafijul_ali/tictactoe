import os
from datetime import datetime

import PySimpleGUI as gui

INTRO_MSG = """					Hello there,

•It is a basic tic-tac-toe game you can play against any other human.
•Fully written in python3.

N.B:- This program doesn't let you play Human v/s Human mode remotely.Those are future plans."""

RULES = """
RULES
•By default symbol for Player 1 is X and for  Player 2 is O

•The board is a 3x3 grid select any of the grid to place your symbol.

•The game moves from Player 1 to Player 2 in sequential turn.

Thanks for playing ^_^ ."""

# variables and gui elements defined here

CONTACT = """
Developer:-Hafiz

Contacts
----------
telegram:-https://t.me/Hafijul_ali
REPL:-repl.it/@HafijulAli
email:-Hafizali0303@yandex.com or zetavs001@protonmail.com
"""
gui.theme("Material 2")
x_score = 0
o_score = 0
player1_name = ""
player2_name = ""

WINDOW_WIDTH = WINDOW_HEIGTH = 500

if os.name == "nt":
    X_IMG = ".\\assets\\X.png"
    O_IMG = ".\\assets\\O.png"
else:
    X_IMG = "./assets/X.png"
    O_IMG = "./assets/O.png"


def time_stamp():
    current_time = datetime.now()
    return current_time.strftime("%m %d %Y, %H:%M:%S")


def get_player_names():
    global player1_name, player2_name
    player1_name = gui.popup_get_text(
        "Enter Player 1 name (X) :", keep_on_top=True)
    player2_name = gui.popup_get_text(
        "Enter Player 2 name (O) :", keep_on_top=True)


def write_log(statement):
    with open("logs.txt", "a") as log:
        log.write("[{0}] {1} \n".format(time_stamp(), statement))


def wipe_log_file():
    with open("logs.txt", "w"):
        pass


def write_save_file():
    global grid
    with open("game_data.txt", "w") as save_file:
        for row in grid:
            for col in row:
                save_file.write(col)
            save_file.write("\n")


def save_game():
    return_value = gui.popup_yes_no(
        "", "Are you sure you want to save?", keep_on_top=True)
    if return_value == "Yes":
        write_save_file()


def load_game():
    global grid, event
    return_value = gui.popup_yes_no(
        "", "Are you sure you want to load?", keep_on_top=True)
    if return_value == "Yes":
        try:
            grid = []
            with open("game_data.txt", "r") as saved_data:
                data_list = saved_data.read().splitlines()
            for data in data_list:
                grid.append(list(data))
            for cell_index in cell_indices:
                row, col = cell_index
                window[cell_index].update(grid[row][col])
        except FileNotFoundError:
            gui.popup(
                "No saved data found, kindly save and then try!,Restarting Game", keep_on_top=True)
            event = "Restart Game"


def change_UI_theme():
    global Theme, event
    change_UI_window = gui.Window("Theme Chooser", [[gui.Text("Select Theme:"),
                                                     gui.Combo(values=gui.theme_list(), size=(30, 1),
                                                               enable_events=True, key="Combo")],
                                                    [gui.Ok(), gui.Cancel()]], size=(300, 300), auto_size_text=True)
    while True:
        ui_event, theme_values = change_UI_window.read(timeout=1000)
        if ui_event in ("Close", gui.WINDOW_CLOSED):
            break
        if ui_event == "Ok":
            Theme = theme_values["Combo"]
            break
    change_UI_window.close()
    gui.theme(Theme)
    event = "Restart Game"


def ckV():
    Wsymb = ""
    global grid
    if (grid[0][0] == grid[1][0]) and (grid[1][0] == grid[2][0]) and grid[2][0] != " ":
        Wsymb = grid[2][0]
    elif (grid[0][1] == grid[1][1]) and (grid[1][1] == grid[2][1]) and grid[2][1] != " ":
        Wsymb = grid[2][1]
    elif (grid[0][2] == grid[1][2]) and (grid[1][2] == grid[2][2]) and grid[2][2] != " ":
        Wsymb = grid[2][2]
    return Wsymb


def ckH():
    global grid
    Wsymb = ""
    if (grid[0][0] == grid[0][1]) and (grid[0][1] == grid[0][2]) and grid[0][2] != " ":
        Wsymb = grid[0][2]
    if (grid[1][0] == grid[1][1]) and (grid[1][1] == grid[1][2]) and grid[1][2] != " ":
        Wsymb = grid[1][2]
    if (grid[2][0] == grid[2][1]) and (grid[2][1] == grid[2][2]) and grid[2][2] != " ":
        Wsymb = grid[2][2]
    return Wsymb


def ckD():
    global grid
    Wsymb = ""
    if (grid[0][0] == grid[1][1]) and (grid[1][1] == grid[2][2]):
        Wsymb = grid[2][2]
    if (grid[0][2] == grid[1][1]) and (grid[1][1] == grid[2][0]):
        Wsymb = grid[2][0]
    return Wsymb


def gameover():
    global count, winner, grid, symbol, x_score, o_score, player1_name, player2_name
    if ckV() == "X" or ckH() == "X" or ckD() == "X":
        winner = player1_name or "X"
        x_score += 1
        window["-scoreboard-"].update(
            "Score: O : {} | X : {} ".format(o_score, x_score))
        return True
    if ckV() == "O" or ckH() == "O" or ckD() == "O":
        winner = player2_name or "O"
        o_score += 1
        window["-scoreboard-"].update(
            "Score: O : {} | X : {} ".format(o_score, x_score))
        return True
    if count == 9:
        winner = None
        return True
    return False


def declare_winner():
    global winner
    if winner:
        gui.popup("Game Over, Winner is:" + winner, keep_on_top=True)
        write_log("Game Over, Winner is : " + winner,)
    else:
        gui.popup("Game Over, It's a draw!", keep_on_top=True)
        write_log("Game Over, Its a draw")
    return "Restart Game"


def place_symbol():
    global count
    x, y = event
    if grid[x][y] == " ":
        grid[x][y] = symbol
        if symbol == "X":
            window[event].update(image_filename=X_IMG)
            write_log(player1_name+": X")
        else:
            window[event].update(image_filename=O_IMG)
            write_log(player2_name+": O")
        count += 1
    else:
        gui.popup("Please select a different Place", keep_on_top=True)


# if player1_name is not None and player2_name is not None:
#     get_player_names()


def close_properly():
    write_save_file()
    wipe_log_file()


while True:
    Theme = "Material 2"
    menu_def = [["File", ["Save", "Load", "Exit"]], ["Options", ["Change Theme", "Restart Game"]],
                ["About", ["Help", "Rules", "Contact"]]]
    layout = [
        [gui.Menu(menu_definition=menu_def)], [gui.Text("Tic-Tac-Toe Game",
                                                        font=("Arial", 20), size=(WINDOW_WIDTH, 1), justification="center")],
        [gui.Text("Score: O : {} | X : {} ".format(o_score, x_score), font=(
            "Arial", 20), size=(WINDOW_WIDTH, 1), justification="center", key="-scoreboard-")]
    ]
    layout += [[gui.Button("", size=(15, 8), auto_size_button=False, key=(x, y), change_submits=True)
                for y in range(3)] for x in range(3)]
    window = gui.Window("Tic-Tac-Toe Game by Hafiz", layout,
                        size=(WINDOW_WIDTH, WINDOW_HEIGTH), default_element_size=(15, 8), auto_size_buttons=False, element_justification="center")
    grid = [[" " for x in range(3)] for y in range(3)]
    cell_indices = [(y // 3, y % 3) for y in
                    range(9)]  # List comprehension :) generates a list of values :- (0,0),(0,1),... (2,2)
    count = 0

    while True:
        if count % 2 == 0:
            symbol = "X"
        else:
            symbol = "O"

        event, values = window.read()

        if event in (gui.WIN_CLOSED, "Exit"):
            close_properly()
            break
        if event == "Save":
            save_game()
        if event == "Load":
            load_game()
        if event == "Change Theme":
            change_UI_theme()
        if event == "Restart Game":
            break
        if event == "Help":
            gui.popup(" ", INTRO_MSG, keep_on_top=True)
        if event == "Rules":
            gui.popup("", RULES, keep_on_top=True)
        if event == "Contact":
            gui.popup("", CONTACT, keep_on_top=True)
        if event in cell_indices:
            place_symbol()
        if gameover():
            event = declare_winner()
            break
    window.close()
    if event == "Restart Game":
        continue
    break
